import "deseq_analysis.wdl" as deseq_analysis
import "utils.wdl" as utils


workflow demo_bam {

    File tsv
    File genes_gtf
    String prefix
    String log_base_cutoff
    String deg_padj_cutoff
    String deg_lfc_cutoff
    String pathway_padj_cutoff
    File jaspar_db_meme
    String genome_version
    String htseq_strand
    File pathways_db_file

    Array[Array[String]] output_table = read_tsv(tsv)

    # tarnspose the matrix to access columns instead of rows
    Array[Array[String]] m = transpose(output_table)

    # get file/fasta map:
    call utils.format as form {
        input: m1 = m[1], m2 = m[0]
    }

    # scatter across the files
    scatter (pair in form.mapping){

        call utils.getarr {
            input: str = pair.right
        }

        # count gene expression using htseq-counts
        call get_htseq_counts {
            input:  bam_file    = getarr.arr[0],
                    bam_index   = getarr.arr[1],
                    genes_gtf   = genes_gtf,
                    strand      = htseq_strand
        }
    }

    # get the output files and match the names
    call utils.get_alignment_map{
        input: alignment_files = get_htseq_counts.gene_counts
    }
    Map[String, File] out_mapping = get_alignment_map.out_mapping

    # rnaseq analysis step with input preparation
    call deseq_analysis.deseq_analysis_workflow {
        input:  input_tsv               = tsv,
                alignment_files_map     = out_mapping,
                prefix                  = prefix,
                log_base_cutoff         = log_base_cutoff,
                deg_padj_cutoff         = deg_padj_cutoff,
                pathway_padj_cutoff     = pathway_padj_cutoff,
                deg_lfc_cutoff          = deg_lfc_cutoff,
                jaspar_db_meme          = jaspar_db_meme,
                genome_version          = genome_version,
                pathways_db_file        = pathways_db_file,
                genes_gtf               = genes_gtf
    }

    output {
        File deseq_deg_excel                = deseq_analysis_workflow.deseq_deg_excel
        File deseq_pathways_excel           = deseq_analysis_workflow.deseq_pathways_excel
        Array[File] deseq_promoters_seq     = deseq_analysis_workflow.deseq_promoters_seq
        File deseq_summary                  = deseq_analysis_workflow.deseq_summary
        File deseq_volcano_plot             = deseq_analysis_workflow.deseq_volcano_plot
        File deseq_bg_model_seq             = deseq_analysis_workflow.deseq_bg_model_seq
        File deseq_control_seq              = deseq_analysis_workflow.deseq_control_seq
        Array[File] motifs_sequences_tsv    = deseq_analysis_workflow.motifs_sequences_tsv
        Array[File] motifs_tsv              = deseq_analysis_workflow.motifs_tsv
        Array[File] motifs_html             = deseq_analysis_workflow.motifs_html
        Array[File] bg_model_frequences     = deseq_analysis_workflow.bg_model_frequences
        File combined_excel_deg             = deseq_analysis_workflow.combined_excel_deg
        Array[File] gene_counts             = get_htseq_counts.gene_counts
    }
}


task get_counts {

    String prefix
    File bam_file
    File bam_index
    File genes_gtf
    String filename = basename(bam_file)

    command {
        rnaseqc ${genes_gtf} ${bam_file} --rpkm --coverage ${prefix};
        mv ${prefix}/${filename}.gene_fragments.gct ${prefix}.gene_fragments.gct;
        mv ${prefix}/${filename}.gene_reads.gct ${prefix}.gene_reads.gct;
        mv ${prefix}/${filename}.gene_rpkm.gct ${prefix}.gene_rpkm.gct;
        mv ${prefix}/${filename}.exon_reads.gct ${prefix}.exon_reads.gct;
    }

    output {
        File gene_fragments = "${prefix}.gene_fragments.gct"
        File gene_counts = "${prefix}.gene_reads.gct"
        File exon_reads = "${prefix}.exon_reads.gct"
        File gene_rpkm = "${prefix}.gene_rpkm.gct"
    }

    runtime {
        docker: "gcr.io/broad-cga-aarong-gtex/rnaseqc:latest"
    }

}


task get_htseq_counts {

    File bam_file
    File bam_index
    File genes_gtf
    String strand
    # TopHat / Cufflinks / Cuffdiff: library-type fr-firststrand
    # HISAT2: --rna-strandedness R (for SE) / RF (for PE)
    # HTSeq: stranded -- reverse
    # for more information visit https://chipster.csc.fi/manual/library-type-summary.html
    String output_filename = sub(basename(bam_file), ".bam", "")

    command {
        htseq-count -f bam -s ${strand} -t gene ${bam_file} ${genes_gtf} > ${output_filename}_gene_counts.txt
    }

    output {
        File gene_counts = "${output_filename}_gene_counts.txt"
    }

    runtime {
        docker: "docker.io/shcheran/rnaseq_demo_htseq:0.1"
        disks: "local-disk 15 SSD"
        cpu: "2"
    }

}