import "rnaseq_pipeline_fastq_modified.wdl" as rnaseq_pipeline
import "deseq_analysis.wdl" as deseq_analysis
import "utils.wdl" as utils


workflow demo_fastq {

    File tsv
    File genes_gtf
    String output_prefix
    String bowtie_index_tar
    String library_type
    String processors
    String log_base_cutoff
    String deg_padj_cutoff
    String deg_lfc_cutoff
    String pathway_padj_cutoff
    File jaspar_db_meme
    String genome_version
    String htseq_strand

    File pathways_db_file

    Array[Array[String]] output_table = read_tsv(tsv)

    # tarnspose the matrix to access columns instead of rows
    Array[Array[String]] m = transpose(output_table)

    # get file/fasta map:
    call utils.format as form {
        input: m1 = m[1], m2 = m[0]
    }

    # scatter across the files and run rnaseq pipeline:
    # tophat alignment, mak of duplicates, samtools indexing, rnaseq counts
    scatter (pair in form.mapping){

        call utils.getarr {
            input: str = pair.right
        }

        call rnaseq_pipeline.rnaseq_pipeline_fastq_workflow{
            input: sample_prefix        = pair.left,
                   fastq1               = getarr.arr[0],
                   fastq2               = getarr.arr[1],
                   bowtie_index_tar     = bowtie_index_tar,
                   library_type         = library_type,
                   processors           = processors,
                   genes_gtf            = genes_gtf,
                   htseq_strand         = htseq_strand
        }
    }

    # get the output files and match the names
    call utils.get_alignment_map{
        input: alignment_files = rnaseq_pipeline_fastq_workflow.gene_counts
    }
    Map[String, File] out_mapping = get_alignment_map.out_mapping

    # deseq analysis: DESeq2, pathway enrichment analysis,
    call deseq_analysis.deseq_analysis_workflow {
        input:  input_tsv                   = tsv,
                alignment_files_map         = out_mapping,
                prefix                      = output_prefix,
                log_base_cutoff             = log_base_cutoff,
                deg_padj_cutoff             = deg_padj_cutoff,
                pathway_padj_cutoff         = pathway_padj_cutoff,
                deg_lfc_cutoff              = deg_lfc_cutoff,
                jaspar_db_meme              = jaspar_db_meme,
                genome_version              = genome_version,
                pathways_db_file            = pathways_db_file,
                genes_gtf                   = genes_gtf
    }

    output {
        File deseq_deg_excel                = deseq_analysis_workflow.deseq_deg_excel
        Array[File] deseq_promoters_seq     = deseq_analysis_workflow.deseq_promoters_seq
        File deseq_pathways_excel           = deseq_analysis_workflow.deseq_pathways_excel
        File deseq_summary                  = deseq_analysis_workflow.deseq_summary
        File deseq_volcano_plot             = deseq_analysis_workflow.deseq_volcano_plot
        File deseq_bg_model_seq             = deseq_analysis_workflow.deseq_bg_model_seq
        File deseq_control_seq              = deseq_analysis_workflow.deseq_control_seq
        File combined_excel_deg             = deseq_analysis_workflow.combined_excel_deg
        Array[File] motifs_sequences_tsv    = deseq_analysis_workflow.motifs_sequences_tsv
        Array[File] motifs_tsv              = deseq_analysis_workflow.motifs_tsv
        Array[File] motifs_html             = deseq_analysis_workflow.motifs_html
        Array[File] bg_model_frequences     = deseq_analysis_workflow.bg_model_frequences
        Array[File] bams                    = rnaseq_pipeline_fastq_workflow.bam
        Array[File] bam_indices             = rnaseq_pipeline_fastq_workflow.bam_index
        Array[File] gene_counts             = rnaseq_pipeline_fastq_workflow.gene_counts

     }
}

