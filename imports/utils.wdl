task format {

    Array[String] m1
    Array[String] m2

    command <<<

        python << CODE

        import os

        v1 = "${sep=',' m1}"
        v2 = "${sep=',' m2}"
        x1 = v1.split(',')
        x2 = v2.split(',')

        # two maps of files
        dict = {}
        for j in range(len(x1)):
            if x1[j] in list(dict.keys()):
                if x2[j] not in dict[x1[j]]:
                    dict[x1[j]].append(x2[j])
            else:
                dict.update({x1[j]: [x2[j]]})

        for k in dict.keys():
            print(k + '\t' + ','.join(dict[k]))

        CODE

        >>>

    runtime {
        docker: "docker.io/python:3.8"
    }

    output {
        Map[String, String] mapping = read_map(stdout())
    }

}


task getarr {

    String str

    command {
        echo ${str} | tr ',' '\n'
    }

    runtime {
        docker: "docker.io/ubuntu:latest"
    }

    output {
        Array[String] arr = read_lines(stdout())
    }
}


task get_alignment_map{

    Array[File] alignment_files

    command <<<

        python << CODE

        import os

        files_str = "${sep=',' alignment_files}"
        files = files_str.split(',')
        for f in files:
            fbucket = "gs://" + f.split('/cromwell_root/')[1]
            bs = os.path.basename(f).split('_accepted_hits_marked_duplicates_gene_counts.txt')[0]
            print(bs + '\t' + fbucket)
        CODE

    >>>

    runtime {
        docker: "docker.io/python:3.8"
    }

    output {
        Map[String, File] out_mapping = read_map(stdout())
    }

}

