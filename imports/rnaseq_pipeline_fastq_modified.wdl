workflow rnaseq_pipeline_fastq_workflow {

    File fastq1
    File fastq2
    String processors
    File bowtie_index_tar
    String library_type
    String sample_prefix
    File genes_gtf
    # TopHat / Cufflinks / Cuffdiff: library-type fr-firststrand
    # HISAT2: --rna-strandedness R (for SE) / RF (for PE)
    # HTSeq: stranded -- reverse
    # for more information visit https://chipster.csc.fi/manual/library-type-summary.html>
    String htseq_strand

    call get_tophat_alignment {
        input:  fastq1              = fastq1,
                fastq2              = fastq2,
                library_type        = library_type,
                sample_prefix       = sample_prefix,
                processors          = processors,
                bowtie_index_tar    = bowtie_index_tar
    }

    # mark duplicates
    call get_markduplicates {
        input: bam = get_tophat_alignment.tophat_mapped_bam
    }

    # index bam files
    call get_index {
        input: bam = get_markduplicates.bam_markduplicates
    }

    # count gene expression
    call get_htseq_counts {
        input:  bam_file    = get_index.bam_out,
                bam_index   = get_index.bam_index_out,
                genes_gtf   = genes_gtf,
                strand      = htseq_strand
    }

    output {
        File bam            = get_index.bam_out
        File bam_index      = get_index.bam_index_out
        File gene_counts    = get_htseq_counts.gene_counts
    }
}


task get_tophat_alignment {

    File fastq1
    File fastq2
    String sample_prefix
    File bowtie_index_tar
    String library_type
    String processors

    command {
        mkdir output;
        mkdir bowtie_index;
        tar -xvvf ${bowtie_index_tar} -C bowtie_index --strip-components=1;
        bowtie_index_basename=$(ls bowtie_index/|head -1|cut -f1 -d".");
        tophat2 -p ${processors} --library-type ${library_type} -o output bowtie_index/$bowtie_index_basename ${fastq1} ${fastq2};
        mv output/accepted_hits.bam ${sample_prefix}_accepted_hits.bam;
        mv output/unmapped.bam ${sample_prefix}_unmapped.bam;
        mv output/align_summary.txt ${sample_prefix}_align_summary.txt;
    }

    output {
        File tophat_mapped_bam = "${sample_prefix}_accepted_hits.bam"
        File tophat_unmapped_bam = "${sample_prefix}_unmapped.bam"
        File tophat_summary = "${sample_prefix}_align_summary.txt"
    }

    runtime {
        docker: "docker.io/genomicpariscentre/tophat2:latest"
        disks: "local-disk 40 SSD"
        memory: "8 GB"
        cpu: "${processors}"
    }
}


task get_markduplicates {

    File bam
    String filename = sub(basename(bam), ".bam", "")

    command {
        java -jar /usr/picard/picard.jar MarkDuplicates \
        I=${bam} \
        O=${filename}_marked_duplicates.bam \
        M=${filename}_marked_dup_metrics.txt
    }

    output {
        File bam_markduplicates = "${filename}_marked_duplicates.bam"
        File summary_markduplicates = "${filename}_marked_dup_metrics.txt"
    }

    runtime {
        docker: "docker.io/broadinstitute/picard:latest"
        disks: "local-disk 20 SSD"
    }
}


task get_index {

    File bam
    String filename = basename(bam)

    command {
        cp ${bam} ${filename};
        samtools index ${filename};
    }

    output {
        File bam_out = "${filename}"
        File bam_index_out = "${filename}.bai"
    }

    runtime {
        docker: "docker.io/biocontainers/samtools:v1.9-4-deb_cv1"
        disks: "local-disk 20 SSD"
    }
}


task get_counts {

    String prefix
    File bam_file
    File bam_index
    File genes_gtf
    String filename = basename(bam_file)

    command {
        rnaseqc ${genes_gtf} ${bam_file} --rpkm --coverage ${prefix};
        mv ${prefix}/${filename}.gene_fragments.gct ${prefix}.gene_fragments.gct;
        mv ${prefix}/${filename}.gene_reads.gct ${prefix}.gene_reads.gct;
        mv ${prefix}/${filename}.gene_rpkm.gct ${prefix}.gene_rpkm.gct;
        mv ${prefix}/${filename}.exon_reads.gct ${prefix}.exon_reads.gct;
    }

    output {
        File gene_fragments = "${prefix}.gene_fragments.gct"
        File gene_counts = "${prefix}.gene_reads.gct"
        File exon_reads = "${prefix}.exon_reads.gct"
        File gene_rpkm = "${prefix}.gene_rpkm.gct"
    }

    runtime {
        docker: "gcr.io/broad-cga-aarong-gtex/rnaseqc:latest"
        disks: "local-disk 20 SSD"
    }

}


task get_htseq_counts {

    File bam_file
    File bam_index
    File genes_gtf
    String strand
    # TopHat / Cufflinks / Cuffdiff: library-type fr-firststrand
    # HISAT2: --rna-strandedness R (for SE) / RF (for PE)
    # HTSeq: stranded -- reverse
    # for more information visit https://chipster.csc.fi/manual/library-type-summary.html
    String output_filename = sub(basename(bam_file), ".bam", "")

    command {
        htseq-count -f bam -s ${strand} -t gene ${bam_file} ${genes_gtf} > ${output_filename}_gene_counts.txt
    }

    output {
        File gene_counts = "${output_filename}_gene_counts.txt"
    }

    runtime {
        docker: "docker.io/shcheran/rnaseq_demo_htseq:0.1"
        disks: "local-disk 15 SSD"
        cpu: "2"
    }

}