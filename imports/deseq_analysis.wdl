import "utils.wdl" as utils


workflow deseq_analysis_workflow {

    File input_tsv
    Map[String, File] alignment_files_map

    String prefix
    String log_base_cutoff
    String deg_padj_cutoff
    String deg_lfc_cutoff
    String pathway_padj_cutoff
    String genome_version
    File jaspar_db_meme
    File pathways_db_file
    File genes_gtf

    # read input tsv file
    Array[Array[String]] output_table = read_tsv(input_tsv)

    # tarnspose the matrix to access columns instead of rows
    Array[Array[String]] m = transpose(output_table)

    # get control-treatment/file map
    call utils.format {
        input: m1 = m[2], m2 = m[1]
    }

    # pass parameters to the DEG analysis task
    call utils.getarr as get_ctrl {
        input: str = format.mapping['control']
    }

    # get array of treatment file prefixes
    call utils.getarr as get_tr {
        input: str = format.mapping['treatment']
    }

    # map treatment
    scatter (prefix in get_tr.arr){
        File mapped_treatment = alignment_files_map[prefix]
    }
    Array[File] treatment_samples = mapped_treatment

    # map control
    scatter (prefix in get_ctrl.arr){
        File mapped_control = alignment_files_map[prefix]
    }
    Array[File] control_samples = mapped_control

    # calculate array sizes: treatment
    scatter (file in treatment_samples){
        Float file_size_treatment = size(file, 'GB')
    }
    Array[Float] sizes_treatment = file_size_treatment
    call sum as sum_treatment {input: values = sizes_treatment}

    # calculate array sizes: control
    scatter (file in control_samples){
        Float file_size_control = size(file, 'GB')
    }
    Array[Float] sizes_control = file_size_control
    call sum as sum_control {input: values = sizes_control}

    # call deseq analysis
    call deseq {
        input:  control             = control_samples,
                treatment           = treatment_samples,
                prefix              = prefix,
                log_base_cutoff     = log_base_cutoff,
                deg_padj_cutoff     = deg_padj_cutoff,
                deg_lfc_cutoff      = deg_lfc_cutoff,
                pathway_padj_cutoff = pathway_padj_cutoff,
                genome_version      = genome_version,
                pathways_db_file    = pathways_db_file,
                genes_gtf           = genes_gtf,
                disk_size           = ceil((sum_treatment.out + sum_control.out + 1)*1.5)
    }

    # run motif enrichment
    scatter (fasta in deseq.prom_seq){
        call run_motif_enrichment {
            input:  fasta_promoters = fasta,
                    fasta_bg_model  = deseq.deseq_bg_model_seq,
                    control_seq     = deseq.deseq_control_seq,
                    jaspar_db_meme  = jaspar_db_meme
        }
    }

    # combine results
    call combine_results {
        input:  motifs_sequences_tsv = run_motif_enrichment.motifs_sequences_tsv_out,
                jaspar_db_meme = jaspar_db_meme,
                excel_table_deg = deseq.deseq_deg_tables_excel
    }

    output {

        File deseq_deg_excel                = deseq.deseq_deg_tables_excel
        File deseq_pathways_excel           = deseq.deseq_pathways_excel
        File deseq_summary                  = deseq.deseq_summary
        File deseq_volcano_plot             = deseq.deseq_volcano_plot
        File deseq_bg_model_seq             = deseq.deseq_bg_model_seq
        File deseq_control_seq              = deseq.deseq_control_seq
        Array[File] deseq_promoters_seq     = deseq.prom_seq
        Array[File] motifs_tsv              = run_motif_enrichment.motifs_tsv_out
        Array[File] motifs_sequences_tsv    = run_motif_enrichment.motifs_sequences_tsv_out
        Array[File] motifs_html             = run_motif_enrichment.motifs_html_out
        Array[File] bg_model_frequences     = run_motif_enrichment.bg_model_frequences
        File combined_excel_deg             = combine_results.combined_excel_deg

    }

}


task deseq {

    Array[File] control
    Array[File] treatment
    String prefix
    String log_base_cutoff
    String deg_padj_cutoff
    String deg_lfc_cutoff
    String pathway_padj_cutoff
    String genome_version
    Int disk_size
    File pathways_db_file
    File genes_gtf

    command {
        treatment=$(echo "${sep=',' treatment}");
        control=$(echo "${sep=',' control}");
        mkdir output;
        Rscript /tools/deseq.R output $treatment $control \
        ${prefix} \
        ${log_base_cutoff} \
        ${deg_padj_cutoff} \
        ${deg_lfc_cutoff} \
        ${pathway_padj_cutoff} \
        ${genome_version} \
        ${pathways_db_file} \
        ${genes_gtf};
        echo "Size of the input files: "${disk_size};
    }
    output {
        Array[File] prom_seq        = ["output/${prefix}_deg_prom_sequences.fasta",
                                       "output/${prefix}_upreg_prom_sequences.fasta",
                                       "output/${prefix}_downreg_prom_sequences.fasta"]
        File deseq_deg_tables_excel = "output/${prefix}_analysis_result_DEGs.xlsx"
        File deseq_pathways_excel   = "output/${prefix}_analysis_result_pathways.xlsx"
        File deseq_summary          = "output/${prefix}_summary.txt"
        File deseq_volcano_plot     = "output/${prefix}_volcano_plot.pdf"
        File deseq_bg_model_seq     = "output/${prefix}_bg_model.fasta"
        File deseq_control_seq     = "output/${prefix}_control_seq.fasta"
    }

    runtime {
        docker: "docker.io/shcheran/rnaseq_demo:0.1"
        disks: "local-disk ${disk_size} SSD"
    }
}


task run_motif_enrichment {

    File fasta_promoters
    File fasta_bg_model
    File jaspar_db_meme
    File control_seq
    String filename = sub(basename(fasta_promoters), ".fasta", "")

    command {
        /opt/libexec/meme-5.0.3/fasta-get-markov -m 0 ${fasta_bg_model} ${filename}_bg_model;
        ame --verbose 1 --oc . \
        --scoring avg --method fisher \
        --hit-lo-fraction 0.25 \
        --evalue-report-threshold 10.0 \
        --control ${control_seq} \
        --bfile ${filename}_bg_model \
        --o motif_enrichment ${fasta_promoters} ${jaspar_db_meme};
        mv motif_enrichment/ame.tsv motif_enrichment/${filename}_ame.tsv;
        mv motif_enrichment/sequences.tsv motif_enrichment/${filename}_sequences.tsv;
        mv motif_enrichment/ame.html motif_enrichment/${filename}_ame.html;
        mv ${filename}_bg_model motif_enrichment/${filename}_bg_model;
    }

    output {
        File motifs_tsv_out             = "motif_enrichment/${filename}_ame.tsv"
        File motifs_sequences_tsv_out   = "motif_enrichment/${filename}_sequences.tsv"
        File motifs_html_out            = "motif_enrichment/${filename}_ame.html"
        File bg_model_frequences        = "motif_enrichment/${filename}_bg_model"
    }

    runtime {
        docker: "docker.io/ddiez/meme:latest"
        memory: "16 GB"
        disks: "local-disk 20 SSD"
    }

}


# combine results
task combine_results {

    Array[File] motifs_sequences_tsv
    File jaspar_db_meme
    File excel_table_deg
    String filename_deg = sub(basename(excel_table_deg), '.xlsx', '')

    command {
        mseq=$(echo "${sep=',' motifs_sequences_tsv}");
        Rscript /tools/combine_results.R "output" \
        ${jaspar_db_meme} \
        ${excel_table_deg} \
        $mseq;
    }

    output {
        File combined_excel_deg = "output/${filename_deg}_tfRegs.xlsx"
    }

    runtime {
        docker: "docker.io/shcheran/rnaseq_demo:0.1"
    }

}


# calculating sum of elements in the array
task sum {

  Array[Float] values

  command <<<
    python -c "print(${sep="+" values})"
  >>>

  output {
    Float out = read_float(stdout())
  }

  runtime {
    docker: "docker.io/python:3.8"
  }
}

