# deseq.R 


# get arguments
args <- commandArgs(trailingOnly=TRUE) 
print(args)


# inputs
outdir <- args[1]
group1 <- args[2] # treatment
group2 <- args[3] # control
prefix <- args[4]
logBaseCutoff <- as.numeric(args[5])
padjCutoff <- as.numeric(args[6])
lfcCutoff <- as.numeric(args[7])
pvalueCutoffPathway <- as.numeric(args[8])
genomeVersion <- args[9]
pathwayFile <- args[10]
refGtf <- args[11]


# load the library
library(ReactomePA)
library(org.Hs.eg.db)
library(AnnotationDbi)
library(biomaRt)
library(openxlsx)
library(DESeq2)
library(Biostrings)



# *********************************************************
# Start of the functions
# *********************************************************

# function for getting genes promoter areas
getUpDownStreamGeneRef <- function(data, upstreamBP=2000, downStreamBP=0){
  
  fwd <- data[which(data$strand == "+"), ]
  rev <- data[which(data$strand == "-"), ]
  fwd_tss_start <- fwd$start_position - upstreamBP 
  fwd_tss_end <- fwd$start_position + downStreamBP 
  rev_tss_start <- rev$end_position - downStreamBP 
  rev_tss_end <- rev$end_position + upstreamBP
  
  # negative ranges
  rev_tss_start[rev_tss_start < 0] <- 0
  fwd_tss_start[fwd_tss_start < 0] <- 0
  
  # substitute 
  fwd$promoter_start <- fwd_tss_start
  fwd$promoter_end <- fwd_tss_end
  rev$promoter_start <- rev_tss_start
  rev$promoter_end <- rev_tss_end
  res <- rbind(fwd, rev)
  rownames(res) <- res$gene_id_version
  resOrd <- res[data$gene_id_version, ]
  resOrd
}


# write to fasta
writeFasta <- function(rangesList, genomeVersion, filename){
  # write fasta file
  cat('Saving sequences in fasta format for motif enrichmnet analysis.\n\n')
  gr <- GRanges(rangesList$chromosome, IRanges(rangesList$promoter_start,rangesList$promoter_end))
  gr <- unique(gr)
  
  # omit chrMT
  start(gr[start(gr) < 0]) <- 0
  gr <- gr[!as.character(seqnames(gr)) %in% 'chrMT']
  
  if(genomeVersion == 'hg19'){
    len <- seqlengths(BSgenome.Hsapiens.UCSC.hg19::Hsapiens)
    genome <- BSgenome.Hsapiens.UCSC.hg19::Hsapiens
  }
  if (genomeVersion == 'hg38'){
    len <- seqlengths( BSgenome.Hsapiens.UCSC.hg38::Hsapiens)
    genome <- BSgenome.Hsapiens.UCSC.hg38::Hsapiens
  }
  
  # trim
  lenR <- len[as.character(seqnames(gr))]
  if(sum(end(gr[!is.na(lenR)]) > lenR[!is.na(lenR)]) > 0){
    cat('Ranges out of chromosome range: ', sum(end(gr[!is.na(lenR)]) > lenR[!is.na(lenR)]), ' - discard')
    end(gr[!is.na(lenR)]) <= lenR[!is.na(lenR)]
    grFilt <- gr[which(!is.na(lenR) & end(gr) <= lenR)]
  } else {
    grFilt <- gr
  }
  
  # save sequences
  sequences <- suppressWarnings(DNAStringSet(getSeq(genome, grFilt, as.character=TRUE)))
  sequences <- as.data.frame(sequences)[,1]
  tmp <- as.data.frame(gr)[,c(1:3)]
  nms <- paste0(tmp[,1], ":", tmp[,2], "-", tmp[,3])
  names(sequences) <- nms
  write(paste0(">", names(sequences), "\n", sequences), file = filename)
}


pathEnrich <- function(pvalueCutoffPathway, gns, degsEntrezID, genomeVersion, pathwayFile){
  
  # file with pathways
  if (length(degsEntrezID) == 0){
    return(data.frame())
  }
  pathways <- read.delim(pathwayFile, header = T, sep = "\t", stringsAsFactors = F)
  pathways$id <- paste0('pathway_', 1:nrow(pathways))
  rownames(pathways) <- pathways$id
  
  # overlaps
  gnsNoDups <- gns[which(!duplicated(gns$entrezgene_id) & !is.na(gns$entrezgene_id)), ]
  gns_all_entrezgene <- as.character(unique(gnsNoDups$entrezgene_id))
  rownames(gnsNoDups) <- gnsNoDups$entrezgene_id
  lst <- lapply(1:nrow(pathways), function(i) strsplit(pathways$entrez_gene_ids[i], ',')[[1]])
  m <- sapply(1:length(lst), function(i) length(lst[[i]]))
  n <- sapply(1:length(lst), function(i) length(setdiff(gns_all_entrezgene, lst[[i]])))
  k <- length(degsEntrezID)
  ov <- sapply(1:length(lst), function(i) intersect(degsEntrezID, lst[[i]]))
  names(ov) <- paste0(pathways$id, ";")
  q <- sapply(1:length(ov), function(i) length(ov[[i]]))
  pval <- sapply(1:length(lst), function(i) phyper(q[i], m[i], n[i], k, lower.tail = FALSE, log.p = FALSE))
  padj <- p.adjust(pval, method = "BH")
  
  # combine into a final table
  v <- unlist(ov)
  if (length(v) == 0){
    return(data.frame())
  }
  dat <- data.frame(p = do.call(rbind, strsplit(names(v), ";"))[, 1], gnsNoDups[v, ], stringsAsFactors = F)
  aggr <- aggregate(dat$hgnc_symbol, list(dat$p), paste0, collapse = '/')
  aggr$Group.1 <- as.character(aggr$Group.1)
  rownames(aggr) <- aggr$Group.1
  res <- data.frame(pathways[, 1:(ncol(pathways)-2)], 
                    pval = as.numeric(format(pval, digits = 3)), 
                    padj = as.numeric(format(padj, digits = 3)), 
                    overlap=paste0(q, '/', m), 
                    geneID = aggr[pathways$id, 'x'], m, stringsAsFactors = F)
  
  # filter
  resFilt <- res[res$m > 1, ]
  resFilt <- resFilt[order(resFilt$padj), ]
  resFilt <- resFilt[resFilt$padj < pvalueCutoffPathway, ]
  rownames(resFilt) <- NULL
  resFilt <- resFilt[,1:(ncol(resFilt)-1)]
  colnames(resFilt) <- c("Description", "ExternalID", "Source", "pval", "padj", "Overlap", "geneID")
  return(resFilt)
}


# volcano plot
plotVolcanoDeseq <- function(res, genomeVersion = "hg19", main = "Volcano plot", lfcThr = 1, 
                             padjThr = 0.05, addText = F, cex = 0.6, sig.lfc = 2, sig.pval = 1e-05, 
                             filtersMap = "ensembl_gene_id", data_attr_name_filter = "ensembl_gene_id", 
                             filterOutlierPvals = FALSE, pvalThrMin = 1e-50, filterByExpression = T, 
                             log2ExprThr = 6, baseMeanVar = "baseMean"){
  
  res <- res[which(!is.na(res$padj)), ]
  res <- res[order(-log10(res$padj), decreasing = T), ]
  
  if(filterOutlierPvals){
    resPlot <- res[res$padj > pvalThrMin, ]
  } else {
    resPlot <- res
  }
  
  # filter by expression
  if(filterByExpression){
    cat("Filtering by expression.\n")
    resPlot <- resPlot[(log2(resPlot$baseMean+1) > log2ExprThr), ]
  }
  
  plot(resPlot$log2FoldChange, -log10(resPlot$padj),
       xlab = "log2 fold change", ylab="-log10(p-value)", 
       main = main, pch = 21, bg = "steelblue", stroke = NULL, 
       cex = 1.1)
  
  resFilt <- res[which(abs(res$log2FoldChange ) > lfcThr & res$padj < padjThr), ]
  rownames(resFilt) <- resFilt[, data_attr_name_filter]
  
  # mark points on the plot
  points(resFilt$log2FoldChange,-log10(resFilt$padj), bg = "orange", stroke = NULL, pch = 21)
  if(addText){
    resFiltSignif <- resFilt[which(abs(resFilt$log2FoldChange ) > sig.lfc & resFilt$padj < sig.pval), ]
    
    # order and select
    resFiltSignif2 <- resFiltSignif[order(abs(resFiltSignif$log2FoldChange), decreasing = T), ]
    resFiltSignifText1 <- resFiltSignif[1:min(nrow(resFiltSignif), 200), ]
    resFiltSignifText2 <- resFiltSignif2[1:min(nrow(resFiltSignif2), 30), ]
    resFiltSignifText <- rbind(resFiltSignifText1, resFiltSignifText2)
    resFiltSignifText <- unique(resFiltSignifText)
    cat('Plotting ', nrow(resFiltSignifText), ' genes')
    
    if(filterOutlierPvals){
      resFiltSignifPlot <- resFiltSignifText[resFiltSignifText$padj > pvalThrMin, ]
    } else {
      resFiltSignifPlot <- resFiltSignifText
    }
    points(resFiltSignif$log2FoldChange,  -log10(resFiltSignif$padj), bg = "red", stroke = NULL, pch = 21)
    rr <- resFiltSignifPlot[resFiltSignifPlot$log2FoldChange > 0, ]
    rl <- resFiltSignifPlot[resFiltSignifPlot$log2FoldChange < 0, ]
    if(nrow(rr) > 0){
      text(rr$log2FoldChange,  -log10(rr$padj), col = "grey30", 
           cex = cex, labels = rr$gene_name, pos = 4)
    }
    if(nrow(rl) > 0){
      text(rl$log2FoldChange,  -log10(rl$padj), col = "grey30", 
           cex = cex, labels = rl$gene_name, pos = 2)
    }
  }
}


# match pathways to genes
matchPathways <- function(pathways, degDat){
  
  if (nrow(degDat) == 0 || nrow(pathways) == 0){
    return(data.frame())
  }
  
  # add marker
  lst <- strsplit(pathways$geneID, "/")
  names(lst) <- pathways$Description
  # degDat[degDat$gene %in% unique(do.call(c, lst)), 'pathway_enrichment'] <- TRUE
  
  # pathways in which a gene was enriched
  df <- do.call(rbind, lapply(1:length(lst), function(k) data.frame(p = names(lst)[k], g = lst[[k]])))
  dfFilt <-  df[df$g %in% degDat$gene_name, ]
  dfFilt$p <- as.character(dfFilt$p); dfFilt <- unique(dfFilt)
  aggr3 <- aggregate(dfFilt$p, list(dfFilt$g), paste0, collapse = ';')
  rw <- degDat$gene_name
  rw[duplicated(rw)] <- paste0(rw[duplicated(rw)], "_dup", 1:length(rw[duplicated(rw)]))
  rownames(degDat) <- rw
  degDat[as.character(aggr3$Group.1), 'pathway'] <- aggr3$x
  degDat <- degDat[order(degDat$padj), ]
  return(degDat)
}





# *********************************************************
# Start the analysis
# *********************************************************


# read gtf file
cat('Reading refence gtf file')
ref <- read.table(refGtf, F, sep = "\t", stringsAsFactors = F)
gns <- ref[which(ref[, 3] == 'gene'), ]
lst <- strsplit(gns[, 9], ";")
# gene id
gene_id_version <- sapply(1:length(lst), function(k) 
  strsplit(grep('gene_id', lst[[k]], value = T), 'gene_id ')[[1]][2])
gene_id <- do.call(rbind, strsplit(gene_id_version, '\\.'))[, 1]
# gene name
gene_name <- sapply(1:length(lst), function(k) 
  strsplit(grep('gene_name', lst[[k]], value = T), 'gene_name ')[[1]][2])
# gene type
gene_type <- sapply(1:length(lst), function(k) 
  strsplit(grep('gene_type', lst[[k]], value = T), 'gene_type ')[[1]][2])
# add chromosome
if (!'chr' %in% gns[1, 1]){chr <- paste0('chr', gns[, 1])}
gnsRef <- data.frame(
  chromosome = chr, start_position = gns[, 4],
  end_position = gns[, 5], strand = gns[, 7], 
  gene_id_version, gene_id, gene_name, 
  gene_type, stringsAsFactors = F)


# get promoter regions
gnsRefProm <- getUpDownStreamGeneRef(gnsRef, upstreamBP = 2000, downStreamBP = 0)


# Get full description to the reference genes including Entrez IDs for pathway enrichment
if (genomeVersion == 'hg19') {
  mart <- useMart(biomart="ENSEMBL_MART_ENSEMBL", host="grch37.ensembl.org", path="/biomart/martservice" , 
                  dataset="hsapiens_gene_ensembl")
} else if (genomeVersion == 'hg38'){
  mart <- useMart("ensembl", dataset="hsapiens_gene_ensembl", host = "www.ensembl.org")
}
# search the table
mapping <- getBM(attributes = c("entrezgene_id", "description", "ensembl_gene_id", "hgnc_symbol"), 
                 filters = "ensembl_gene_id", values = gnsRefProm$gene_id, mart = mart, 
                 uniqueRows = T, useCache = FALSE)
mapping <- mapping[!duplicated(mapping$ensembl_gene_id), ]
rownames(mapping) <- mapping$ensembl_gene_id
mapOrdered <- mapping[gnsRefProm$gene_id, c('entrezgene_id', 'hgnc_symbol', 'description')]
gnsRefProm <- data.frame(gnsRefProm, mapOrdered, stringsAsFactors = F)
gnsRefProm$description[gnsRefProm$description == ''] <- NA
gnsRefProm$hgnc_symbol[gnsRefProm$hgnc_symbol == ''] <- NA
rownames(gnsRefProm) <- gnsRefProm$gene_id_version



# combine groups
gr1 <- strsplit(group1, ',')[[1]]
gr2 <- strsplit(group2, ',')[[1]]; names(gr2) = 'group2'
nms1 <- paste0(do.call(rbind, strsplit(basename(gr1), '\\.'))[, 1], "_group1")
nms2 <- paste0(do.call(rbind, strsplit(basename(gr2), '\\.'))[, 1], "_group2")


# description
type <- c(rep('group1', length(gr1)), rep('group2', length(gr2)))
meta <- data.frame(filename = c(gr1, gr2), sample = c(nms1, nms2), type = type, stringsAsFactors = F)


# construct a matrix
grTbl <- read.table(gr1[1], header = F, sep = "\t", skip = 0, stringsAsFactors = F)
colnames(grTbl) <- c('Name', 'Counts')
grTbl <- grTbl[1:(nrow(grTbl) - 5), ]
grTbl$Counts <- as.numeric(grTbl$Counts)
m <- data.frame(gene_id = grTbl$Name, stringsAsFactors = F)
rownames(m) <- m$gene_id


# go through the files
for (i in 1:nrow(meta)){
  experiment = meta[i, ]
  tt <- read.table(experiment$filename, header = F, sep = "\t", skip = 0, stringsAsFactors = F)
  colnames(tt) <- c('Name', 'Counts')
  tt <- tt[1:(nrow(tt) - 5), ]
  tt$Counts <- as.numeric(tt$Counts)
  v <- tt$Counts
  names(v) <- tt$Name
  m <- data.frame(m, sample = v[m$gene_id])
}
cols <- colnames(m)
cols[grep('sample', colnames(m))] <- meta$sample
colnames(m) <- cols
# filter zero rows
mfilt <- m[rowSums(m[, c(3:ncol(m))]) != 0, ]


# factrorize
colDataAll <- data.frame(treatment = factor(meta$type))
expr <- mfilt[, c(2:ncol(mfilt))]
rownames(expr) <- mfilt$gene_id


# DESEQ analysis
cat('Starting Differential Gene Expression analysis (DESeq2)\n\n')
dds <- DESeqDataSetFromMatrix(countData = expr, colDataAll, design = ~ treatment)
ddsTmp <- estimateSizeFactors(dds)
normcounts <- counts(ddsTmp, normalized=TRUE)
normcounts <- round(log2(normcounts + 1), 3)
colnames(normcounts) <- do.call(rbind, strsplit(colnames(expr), "_accepted_hits"))[, 1]
normcounts <- data.frame(gene_id = rownames(normcounts), normcounts, stringsAsFactors = F)
rownames(normcounts) <- NULL
dds <- DESeq(dds)


# get DEG table: treatment vs control
deg <- results(dds, contrast=c("treatment", "group1", "group2"), independentFiltering = F)
degFull <- data.frame(gnsRefProm[rownames(deg), ], deg, stringsAsFactors = F)
degFull$pathway <- NA
cols <- c('chromosome','start_position','end_position','strand','promoter_start','promoter_end',
          'gene_id_version','gene_id','entrezgene_id','gene_name','gene_type','baseMean','log2FoldChange',
          'pvalue','padj','description','pathway')
degFull <- degFull[, cols]
degFull$log2FoldChange <- round(degFull$log2FoldChange, 3)
degFull$baseMean <- round(degFull$baseMean, 3)
degFull$pvalue <- as.numeric(format(degFull$pvalue, digits = 3))
degFull$padj <- as.numeric(format(degFull$padj, digits = 3))


# filter the genes
degFilt <- degFull[which(degFull$padj < padjCutoff), ]
degFilt <- degFilt[log2(degFilt$baseMean + 1) > logBaseCutoff, ]
degFilt <- degFilt[abs(degFilt$log2FoldChange) > lfcCutoff, ]
degFilt <- degFilt[order(degFilt$padj, decreasing = F), ]
degFilt <- degFilt[, cols]
degFilt <- degFilt[which(degFilt$gene_type %in% c("protein_coding", "processed_transcript")), ]


# split into up-regulated and down-regulated
degUp <- degFilt[degFilt$log2FoldChange > 0, ]
degDown <- degFilt[degFilt$log2FoldChange < 0, ]
cat(nrow(degFilt), ' differentially expressed genes found\n\n')
cat(nrow(degUp), ' up-regulated genes\n\n')
cat(nrow(degDown), ' down-regulated genes\n\n')


# pathway enrichment analysis
cat("Getting enriched pathways for all differentially expressed genes\n\n")
pathwaysAll <- pathEnrich(pvalueCutoffPathway, gnsRefProm, degFilt$entrezgene_id, genomeVersion, pathwayFile)
# upreg
pathwaysUP <- pathEnrich(pvalueCutoffPathway, gnsRefProm, degUp$entrezgene_id, genomeVersion, pathwayFile)
# downreg
pathwaysDown <- pathEnrich(pvalueCutoffPathway, gnsRefProm, degDown$entrezgene_id, genomeVersion, pathwayFile)


# select genes in pathways
dseqFullFinal <- matchPathways(pathwaysAll, degFull)
degAllFinal <- matchPathways(pathwaysAll, degFilt)
degUpFinal <- matchPathways(pathwaysUP, degUp)
degDownFinal <- matchPathways(pathwaysDown, degDown)


# saving output pathway tables
cat('Saving results to the excel table 1...\n\n')
foutExcel <- paste0(outdir, '/', prefix, '_analysis_result_DEGs.xlsx')
wb <- createWorkbook()
addWorksheet(wb, "DEG all")
addWorksheet(wb, "DEG up-regulated")
addWorksheet(wb, "DEG down-regulated")
addWorksheet(wb, "DESeq2 results all")
addWorksheet(wb, "normalized counts")
writeData(wb, sheet="DEG all", x = degAllFinal, startCol = 1, 
          startRow = 1, withFilter = FALSE, keepNA = TRUE, sep = ", ")
writeData(wb, sheet="DEG up-regulated", x = degUpFinal, startCol = 1, 
          startRow = 1, withFilter = FALSE, keepNA = TRUE, sep = ", ")
writeData(wb, sheet="DEG down-regulated", x = degDownFinal, startCol = 1, 
          startRow = 1, withFilter = FALSE, keepNA = TRUE, sep = ", ")
writeData(wb, sheet="DESeq2 results all", x = dseqFullFinal, startCol = 1, 
          startRow = 1, withFilter = FALSE, keepNA = TRUE, sep = ", ")
writeData(wb, sheet="normalized counts", x = normcounts, startCol = 1, 
          startRow = 1, withFilter = FALSE, keepNA = TRUE, sep = ", ")
saveWorkbook(wb, foutExcel, overwrite = TRUE)


# save other table
cat('Saving results to the excel table 2...\n\n')
foutExcel <- paste0(outdir, '/', prefix, '_analysis_result_pathways.xlsx')
wb <- createWorkbook()
addWorksheet(wb, "pathway analysis")
addWorksheet(wb, "pathway analysis (upreg)")
addWorksheet(wb, "pathway analysis (downreg)")
writeData(wb, sheet="pathway analysis", x = pathwaysAll, startCol = 1, 
          startRow = 1,  withFilter = FALSE, keepNA = TRUE, sep = ", ")
writeData(wb, sheet="pathway analysis (upreg)", x = pathwaysUP, startCol = 1, 
          startRow = 1,  withFilter = FALSE, keepNA = TRUE, sep = ", ")
writeData(wb, sheet="pathway analysis (downreg)", x = pathwaysDown, startCol = 1, 
          startRow = 1,  withFilter = FALSE, keepNA = TRUE, sep = ", ")
saveWorkbook(wb, foutExcel, overwrite = TRUE)


# save sequences to fasta file
fastaAll <- paste0(outdir, '/', prefix, '_deg_prom_sequences.fasta')
fastaUp <- paste0(outdir, '/', prefix, '_upreg_prom_sequences.fasta')
fastaDown <- paste0(outdir, '/', prefix, '_downreg_prom_sequences.fasta')
writeFasta(degFilt, genomeVersion, fastaAll)
writeFasta(degUp, genomeVersion, fastaUp)
writeFasta(degDown, genomeVersion, fastaDown)


# save sequences for bg model for motif enrichment
cat("Getting background sequences.")
degBG <- dseqFullFinal[order(abs(dseqFullFinal$log2FoldChange), decreasing = F), ]
degBG <- degBG[which(degBG$padj > 0.9), ]
degBG <- degBG[which(abs(degBG$log2FoldChange) < 0.1), ]
cat(nrow(degBG), " sequences chosen for buiding background model")
foutFastaBG <- paste0(outdir, '/', prefix, '_bg_model.fasta')
writeFasta(degBG, genomeVersion, foutFastaBG)


# save sequences for control model for motif enrichment
# gnsControl <- dseqFullFinal[which(dseqFullFinal$gene_type %in% c("protein_coding", "processed_transcript")), ]
cat(nrow(dseqFullFinal), 'selected for control sequences\n')
foutFastaControl <- paste0(outdir, '/', prefix, '_control_seq.fasta')
writeFasta(dseqFullFinal, genomeVersion, foutFastaControl)


# volcano plot
cat('Saving volcano plot\n\n')
fname <- paste0(outdir, '/', prefix, '_volcano_plot.pdf')
pdf(file = fname, width = 7.5, height = 8)
selected <- c("protein_coding", "processed_transcript")
dat <- dseqFullFinal[which(dseqFullFinal$gene_type %in% selected), ]
plotVolcanoDeseq(dat, filtersMap = "ensembl_gene_id", 
                 data_attr_name_filter = "gene_id",  addText = T,
                 sig.lfc = 2, sig.pval = 1e-5)
dev.off()


# summary of the run
summary <- paste0("Summary of the differential expression analysis \n\n", 
                  "Group 1: ", group1, "\n", 
                  "Group 2: ", group2, "\n", 
                  "Prefix: ", prefix, "\n", 
                  "logBaseCutoff: ", logBaseCutoff, "\n", 
                  "padjCutoff: ", padjCutoff, "\n", 
                  "lfcCutoff: ", lfcCutoff, "\n",
                  "pvalueCutoffPathway: ", pvalueCutoffPathway, "\n",
                  "genomeVersion: ", genomeVersion, "\n",
                  "Total number of differentially expressed genes: ", nrow(degAllFinal), "\n",
                  "Number of up-regulated expressed genes: ", nrow(degUpFinal), "\n",
                  "Number of down-regulated expressed genes: ", nrow(degDownFinal), "\n",
                  "Number of enriched pathways using all differentially expressed genes: ", nrow(pathwaysAll), "\n",
                  "Number of enriched pathways using all up-regulated genes: ", nrow(pathwaysUP), "\n",
                  "Number of enriched pathways using all differentially expressed genes: ", nrow(pathwaysDown), "\n")


cat(summary)
foutSummary <- paste0(outdir, '/', prefix, '_summary.txt')
write(summary, foutSummary)


cat('Done!')

