# RNA-seq Analysis Pipeline

This repository contains a [Cromwell workflow manager](https://cromwell.readthedocs.io/en/stable/)-based pipeline aimed at analysis of RNA-seq data. 

## Introduction

The pipeline is aimed at end-to-end analysis of RNA-seq data including reads alignment and processing, gene expression 
counting followed by differential gene expression analysis, pathway enrichment analysis and motif enrichment analysis of 
promoters regions of differentially expressed genes (DEGs). Two versions of the pipeline described below are available in the repository.

**Full version "demo_fastq.wdl"** includes the following analysis steps:
1. Read alignment using [Tophat2](https://ccb.jhu.edu/software/tophat/index.shtml) and [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml) index.
2. Duplicates removal using [Picard](https://broadinstitute.github.io/picard/) tool.
3. Building bam index file using [Samtools](http://samtools.sourceforge.net/) tool.
4. Counting gene expression using [HTseq](https://chipster.csc.fi/manual/library-type-summary.html) using strand direction specified by the user and `-t gene` option.
5. Differential expression analysis using [DESeq2](http://bioconductor.org/packages/devel/bioc/vignettes/DESeq2/inst/doc/DESeq2.html) tool.
6. Pathway enrichment analysis based on list of pathways available at http://cpdb.molgen.mpg.de/ using hyper-geometric 
test in R (phyper) with subsequent Benjamin Hochberg p-value adjustment.
7. Motif enrichment analysis of promoters (2kb upstream of TSS) of differentially expressed genes using [AME](http://meme-suite.org/doc/ame.html) tool.
Motif enrichment is performed based on zero-order background model obtained with [`fasta-get-markov`](http://meme-suite.org/doc/fasta-get-markov.html)
from sequences randomly selected from non-differentially expressed genes and control sequences being represented by sequences collected from all gene promoters.

**Reduced version "demo_bam.wdl"** includes steps 4-7.

### Full pipeline inputs

- *tsv*: a tab-separated sample information file without header is required to run the full extended pipeline. A TSV file should contain three columns, where: 
    - The first column specifies path to a FASTQ file.
    - The second column specifies the sample ID and acts also as the desired name for some output files including alignment, expression counts, etc.
    - The third column must contain either label **control** or **treatment** (NOTE using one of these two labels is compulsory at the moment).
         <table align="center">
              <tr>
                <td align="left">/path/to/s1_reads_1.fastq.gz</td>
                <td align="left">s1</td>
                <td align="left">control</td>
              </tr>
              <tr>
                <td align="left">/path/to/s1_reads_2.fastq.gz</td>
                <td align="left">s1</td>
                <td align="left">control</td>
              </tr>
              <tr>
                <td align="left">/path/to/s2_reads_1.fastq.gz</td>
                <td align="left">s2</td>
                <td align="left">control</td>
              </tr>
              <tr>
                <td align="left">/path/to/s2_reads_2.fastq.gz</td>
                <td align="left">s2</td>
                <td align="left">control</td>
              </tr>
              <tr>
                <td align="left">/path/to/s3_reads_1.fastq.gz</td>
                <td align="left">s3</td>
                <td align="left">treatment</td>
              </tr>
              <tr>
                <td align="left">/path/to/s3_reads_2.fastq.gz</td>
                <td align="left">s3</td>
                <td align="left">treatment</td>
              </tr>
              <tr>
                <td align="left">/path/to/s4_reads_1.fastq.gz</td>
                <td align="left">s4</td>
                <td align="left">treatment</td>
              </tr>
              <tr>
                <td align="left">/path/to/s4_reads_2.fastq.gz</td>
                <td align="left">s4</td>
                <td align="left">treatment</td>
              </tr>
          </table>
- *output_prefix*: prefix that is used for naming all output files;
- *genome_version*: version of the genome used for alignment (e.g. 'hg19', 'hg38');
- *genes_gtf*: gtf file used for reads alignment;
- *bowtie_index_tar*: tar file containing [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml) index *.bt2 files 
(Bowtie2 index can be build using `bowtie2-build Homo_sapiens_assembly19.fasta hg19`);
- *library_type*: RNA-seq experiment library type (read more at https://chipster.csc.fi/manual/library-type-summary.html);
- *htseq_strand*: the information on the strand required for gene expression calculation perfomed by HTSeq (read more at 
https://chipster.csc.fi/manual/library-type-summary.html);
- *jaspar_db_meme*: JASPAR DB file to be subjected for motif enrichment analysis (See [Example](#test-example-based-on-prostate-cancer-rna-seq-dataset)),
- *pathways_db_file*: pathways file to be checked for enrichment with columns *pathway*, *external_id*, *source*, *entrez_gene_ids*, 
where *entrez_gene_ids* contains comma-separated list of Entrez gene IDs (See [Example](#test-example-based-on-prostate-cancer-rna-seq-dataset)),
- *log_base_cutoff*: average log2-base expression cutoff across samples to be filtered out for differential expression 
analysis (often set to 6 in RNA-seq studies)
- *deg_padj_cutoff*: adjusted p-value cutoff for selecting differentially expressed genes;
- *deg_lfc_cutoff*: abs(log2-fold change) cutoff for selecting differentially expressed genes;
- *pathway_padj_cutoff*: adjusted p-value cutoff for selecting enriched pathways in pathway enrichment analysis;
- *processors*: number of processors for running TopHat2.

An example of inputs required for the full version of the pipeline submitted to the Cromwell workflow manager in JSON format:
```
{
  "demo_fastq.output_prefix": "de_analysis",
  "demo_fastq.genome_version": "hg19",
  "demo_fastq.library_type": "fr-firststrand",
  "demo_fastq.htseq_strand": "reverse",
  "demo_fastq.genes_gtf": "/path/to/annotation.gtf",
  "demo_fastq.bowtie_index_tar": "/path/to/bowtie2/index/hg19.tar.gz",
  "demo_fastq.pathways_db_file": "/path/to/CPDB_pathways_entrez_genes.tab",
  "demo_fastq.jaspar_db_meme": "/path/to/JASPAR2020_CORE_vertebrates_non-redundant_pfms_meme.txt",
  "demo_fastq.processors": "6",
  "demo_fastq.log_base_cutoff": "6",
  "demo_fastq.deg_padj_cutoff": "0.05",
  "demo_fastq.deg_lfc_cutoff": "1",
  "demo_fastq.pathway_padj_cutoff": "0.05"  
}
```

### Reduced pipeline inputs
- *tsv*: a tab-separated sample information file without header is required to run the full extended pipeline. A TSV file should contain three columns, where: 
    - The first column specifies path to a BAM file.
    - The second column specifies the sample ID and acts also as the desired name for some output files including alignment, expression counts, etc.
    - The third column must contain either label **control** or **treatment** (NOTE using one of these two labels is compulsory at the moment).
      <table align="center">
          <tr>
            <td align="left">/path/to/s1.bam</td>
            <td align="left">s1</td>
            <td align="left">control</td>
          </tr>
          <tr>
            <td align="left">/path/to/s1.bam.bai</td>
            <td align="left">s1</td>
            <td align="left">control</td>
          </tr>
          <tr>
            <td align="left">/path/to/s2.bam</td>
            <td align="left">s2</td>
            <td align="left">control</td>
          </tr>
          <tr>
            <td align="left">/path/to/s2.bam.bai</td>
            <td align="left">s2</td>
            <td align="left">control</td>
          </tr>
          <tr>
            <td align="left">/path/to/s2.bam</td>
            <td align="left">s3</td>
            <td align="left">treatment</td>
          </tr>
          <tr>
            <td align="left">/path/to/s3.bam.bai</td>
            <td align="left">s3</td>
            <td align="left">treatment</td>
          </tr>
          <tr>
            <td align="left">/path/to/s4.bam</td>
            <td align="left">s4</td>
            <td align="left">treatment</td>
          </tr>
          <tr>
            <td align="left">/path/to/s4.bam.bai</td>
            <td align="left">s4</td>
            <td align="left">treatment</td>
          </tr>
      </table>
- The rest of the inputs the same provided to the extended pipeline version except *bowtie_index_tar*, *library_type*, 
*processors* since they are only required for reads alignment.


An example of inputs required for the reduced version of the pipeline submitted to the Cromwell workflow manager in JSON format:
```
{
  "demo_bam.output_prefix": "de_analysis",
  "demo_bam.genome_version": "hg19",
  "demo_bam.htseq_strand": "reverse",
  "demo_bam.genes_gtf": "/path/to/annotation.gtf",
  "demo_bam.pathways_db_file": "/path/to/CPDB_pathways_entrez_genes.tab",
  "demo_bam.jaspar_db_meme": "/path/to/JASPAR2020_CORE_vertebrates_non-redundant_pfms_meme.txt",
  "demo_bam.log_base_cutoff": "6",
  "demo_bam.deg_padj_cutoff": "0.05",
  "demo_bam.deg_lfc_cutoff": "1",
  "demo_bam.pathway_padj_cutoff": "0.05"  
}
```

Imported modules are zipped and submitted to the Cromwell tools using `-F workflowDependencies=@imports/imports.zip`, inputs listed 
in the JSON file and submitted to via `-F workflowInputs=@inputs.json`. An example of the full command used to submit job to Cromwell 
workflow manager running in a server mode using default Cromwell's interface and port (which can be reset in a Cromwell configuration file):
```
curl -X POST http://0.0.0.0:8000/api/workflows/v1 \
-F workflowSource=@demo_fastq.wdl \
-F workflowInputs=@inputs.json \
-F workflowDependencies=@imports/imports.zip
```


### Pipeline outputs

Depending on whether reduced or full version of the pipeline was executed, pipeline's outputs include the following files:
- Alignment bam and index files;
- Gene expression counts;
- Excel table containing DEGs, lists of up-regulated and down-regulated genes, a table with normalized gene counts;
- Excel table containing pathway enrichment results;
- Motif enrichment analysis results including html file containing main results and the complementary tsv tables containing 
detailed information on the analysis results;
- Fasta files containing target and control sequences for motif enrichment analysis;
- Volcano plot of the DEGs.

## Docker containers

**Public**

All public docker images required for the analysis were obtained from the [Docker Hub](https://hub.docker.com/):
- Samtools: `docker.io/biocontainers/samtools:v1.9-4-deb_cv1`
- Tophat2: `docker.io/genomicpariscentre/tophat2:latest`
- Picard: `docker.io/broadinstitute/picard:latest`
- Python: `docker.io/python:3.8`
- AME: `docker.io/ddiez/meme:latest`

**Custom**
- DESeq2 analysis: `docker.io/shcheran/rnaseq_demo:0.1`
- HTSeq with required python dependencies: `docker.io/shcheran/rnaseq_demo_htseq:0.1`

The following commands can be used in order to build custom DESeq2 and HTSeq docker images: 
```
docker build -t deseq:0.1 dockers/deseq/
docker build -t htseq:0.1 dockers/htseq/
```

## Google Cloud Platform integration

Cromwell workflow management tool was setup to run in the server mode on the Google Cloud Compute Engine standard VM. 
A _n1-standard-1_ (1 vCPU, 3.75 GB memory) Compute Engine VM instance was created to run Cromwell server. 
The VM was configured in  the _europe-west1-b_ zone. Compute Engine Default service account was used in the Cromwell's 
configuration along with enabled call-caching feature, configured Genomics Pipeline API enabled at the backend and default 
runtime attributes. 

The following variables were used in the Cromwell's configuration:
- _APP_NAME_: preferable name of the app;
- _PROJECT_ID_: Google Cloud Platform project ID;
- _COMPUTE_ENGINE_DEFAULT_SERVICE_ACCOUNT_ID_: Compute Engine Default service account specified in the form `<PROJECT_NUMBER>-compute@developer.gserviceaccount.com`
- _SERVICE_ACCOUNT_PEM_FILE_: PEM file containing the key for the service account which is obtained from the JSON key credentials. 
These credentials can be obtained by going to the Navigation Menu in the **Cloud Console** -> **IAM & Admin** -> **Service Accounts**. 
Choose the service account which will be used to run Cromwell server (Compute Engine Default service account) and by clicking
**Actions** and selecting **Create key** from the dropdown menu create a key JSON for the service account. Open the created 
credentials file and generate a [PEM file](https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail) from the private key. Same 
PEM file in your VM running the Cromwell server.
- _CROMWELL_OUTPUT_BUCKET_: a Google Storage bucket generated for storing the outputs of the Cromwell tool.
- _AUTH_NAME_: name for the authentication element, e.g "cromwell-server".
 
The following configurations were enabled:
```
google {
  application-name = "APP_NAME"
  auths = [
    {
      name = "AUTH_NAME"
      scheme = "service_account"
      pem-file = "SERVICE_ACCOUNT_PEM_FILE"
      service-account-id = "COMPUTE_ENGINE_DEFAULT_SERVICE_ACCOUNT_ID"
    }
  ]
}
call-caching {
  enabled = true
  invalidate-bad-cache-results = true
}
backend {
  default = "PAPIv2"
  providers {
    PAPIv2 {
      actor-factory = "cromwell.backend.google.pipelines.v2alpha1.PipelinesApiLifecycleActorFactory"
      config {
        project = "PROJECT_ID"
        root = "CROMWELL_OUTPUT_BUCKET"
        genomics {
          auth = "AUTH_NAME"
          compute-service-account = "COMPUTE_ENGINE_DEFAULT_SERVICE_ACCOUNT_ID"
          endpoint-url = "https://genomics.googleapis.com/"
          restrict-metadata-access = true
        }
        filesystems {
          gcs {
            auth = "AUTH_NAME"
            caching {
              duplication-strategy = "copy"
            }
          }
        }
        default-runtime-attributes {
          memory: "4G"
          preemptible: 2
          bootDiskSizeGb: 10
          zones: "europe-west1-b"
        }
      }
    }
  }
}

```

## Test example based on prostate cancer RNA-seq dataset

Pipeline was tested using dataset publicly available at European Nucleotide Archive (study ID: [PRJNA474819](https://www.ebi.ac.uk/ena/data/view/PRJNA474819)) which 
contains RNA-Seq measurements of prostate cancer cell line LNCaP. Three LNCaP replicates treated with androgen were
compared against three untreated LNCaP replicates. In the example the following inputs were used:

[Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml) index provided to the pipeline was built based on the fasta 
reference [Homo_sapiens_assembly19.fasta](http://www.broadinstitute.org/ftp/pub/seq/references/Homo_sapiens_assembly19.fasta).

The pipeline was executed using the following parameters/input reference files:
- Genome version hg19.
- GTF annotation [gencode.v19.annotation.gtf](https://www.encodeproject.org/files/gencode.v19.annotation/@@download/gencode.v19.annotation.gtf.gz).
- Sequencing library type `fr-firststrand` specified for performing reads alignment.
- Strand direction `reverse` specified for counting genes expression performed by HTSeq (more information on library type 
and strand direction for HTseq tool can be found at https://chipster.csc.fi/manual/library-type-summary.html).
- Motif DB [JASPAR2020 CORE vertebrates non redundant](http://jaspar.genereg.net/download/CORE/JASPAR2020_CORE_vertebrates_non-redundant_pfms_meme.txt) 
for running motif enrichment analysis.
- Biological pathways (as defined by source databases) with their genes identified with Entrez gene obtained from 
http://cpdb.molgen.mpg.de/CPDB/daccess website. The file is a four-columns tab-delimited table containing 
*pathway*, *external_id*, *source*, *entrez_gene_ids* columns where *entrez_gene_ids* is comma-separated list of Entrez gene IDs.

For obtaining a list of DEGs an adjusted p-value cutoff of 0.01 and log2FC cutoff of 1 was used. For obtaining a list 
enriched pathways a p-value cutoff of 0.01 was used. And example input tsv file:
```
gs://GOOGLE_STORAGE_BUCKET/S1_lncap_control_SRR7268702_1.fastq.gz	s1_lncap_control	control
gs://GOOGLE_STORAGE_BUCKET/S1_lncap_control_SRR7268702_2.fastq.gz	s1_lncap_control	control
gs://GOOGLE_STORAGE_BUCKET/S2_lncap_control_SRR7268703_1.fastq.gz	s2_lncap_control	control
gs://GOOGLE_STORAGE_BUCKET/S2_lncap_control_SRR7268703_2.fastq.gz	s2_lncap_control	control
gs://GOOGLE_STORAGE_BUCKET/S3_lncap_control_SRR7268704_1.fastq.gz	s3_lncap_control	control
gs://GOOGLE_STORAGE_BUCKET/S3_lncap_control_SRR7268704_2.fastq.gz	s3_lncap_control	control
gs://GOOGLE_STORAGE_BUCKET/S4_lncap_ARtreatment_SRR7268705_1.fastq.gz	s4_lncap_treatment_AR	treatment
gs://GOOGLE_STORAGE_BUCKET/S4_lncap_ARtreatment_SRR7268705_2.fastq.gz	s4_lncap_treatment_AR	treatment
gs://GOOGLE_STORAGE_BUCKET/S5_lncap_ARtreatment_SRR7268706_1.fastq.gz	s5_lncap_treatment_AR	treatment
gs://GOOGLE_STORAGE_BUCKET/S5_lncap_ARtreatment_SRR7268706_2.fastq.gz	s5_lncap_treatment_AR	treatment
gs://GOOGLE_STORAGE_BUCKET/S6_lncap_ARtreatment_SRR7268707_1.fastq.gz	s6_lncap_treatment_AR	treatment
gs://GOOGLE_STORAGE_BUCKET/S6_lncap_ARtreatment_SRR7268707_2.fastq.gz	s6_lncap_treatment_AR	treatment
```

### Results

**Differential expression**
AR-treated LNCaP samples were compared against control untreated LNCaP samples. Overall, 1036 genes were found to be 
differentially expressed among which 560 were significantly up-regulated and 476 were down-regulated. Among top 
significantly up-regulated genes were TMPRSS2, KLK2, and KLK3 which are known AR targets, and KLF15 gene. Among top 
significantly down-regulated genes were ST7 and TP53INP1. 

![alt text](volcano_plot.png "Differentially expressed genes")


**Pathway enrichment**
Pathway enrichment analysis was performed as a part of the pipeline for all differentially expressed genes as well as 
for up-regulated and down-regulated lists of genes separately. Among pathways found to be enriched using the list of up-regulated 
genes were "activated PKN1 stimulates transcription of AR (androgen receptor) regulated genes KLK2 and KLK3" and "prostate cancer" 
pathway with gene targets TMPRSS2, KLK3, IGF1, IGF1R,NKX3-1, MTOR, NFKBIA, EGFR, CREB3L2, CREB3L4. In addition, up-regulation of 
pathways associated with cholesterol biosynthesis was observed which is consistent with previous prostate cancer studies.

**Motif enrichment**
Motif enrichment analysis was done using sequences extracted from promoters of differentially expressed genes where promoter 
is defined as a 2kb-long region upstream the gene's Transcription Start Site (TSS). The background frequency is estimated based 
on promoter sequences extracted from non-differentially expressed genes and the control sequences are represented by the promoter 
sequences of genes available in the reference. Motifs enriched in sequences of up-regulated genes included KLF15 which is also found among the up-regulated genes, in addition to TFDP1 and ZBTB14 transcription factors.
